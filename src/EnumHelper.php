<?php

namespace MahanShoghy\PhpEnumHelper;

use BackedEnum;
use Exception;
use ReflectionClass;
use ReflectionMethod;

trait EnumHelper
{
    /**
     * @throws Exception
     */
    public static function __callStatic($name, $args)
    {
        foreach (self::cases() as $case) {
            if ($case->name === $name) {
                return $case instanceof BackedEnum ? $case->value : $case->name;
            }
        }

        throw new Exception(self::class);
    }

    private static function getReflection(): ReflectionClass
    {
        return new ReflectionClass(static::class);
    }

    /**
     * Get array of names
     *
     * @return array
     */
    public static function names(): array
    {
        $array = [];
        foreach (self::cases() as $item){
            $array[] = $item->name;
        }

        return $array;
    }

    /**
     * Get array of values
     *
     * @return array
     */
    public static function values(): array
    {
        $array = [];
        foreach (self::cases() as $item){
            $array[] = $item->value;
        }

        return $array;
    }

    /**
     * Get enum based on specific value
     *
     * @param mixed $value
     * @return static|null
     */
    public static function getFrom(mixed $value): ?self
    {
        foreach(self::cases() as $item) {
            if ($item->value === $value){
                return $item;
            }
        }

        return null;
    }

    /**
     * Get count of enum values
     *
     * @return int
     */
    public static function count(): int
    {
        return count(self::cases());
    }

    /**
     * Check value is valid
     *
     * @param EnumHelper $value
     * @return bool
     */
    public static function isValid(self $value): bool
    {
        return in_array($value, self::cases());
    }

    /**
     * Get array if extra enum methods
     *
     * @return array
     */
    public static function extraMethods(): array
    {
        $items = self::getReflection()->getMethods(ReflectionMethod::IS_PUBLIC);
        $items = array_filter($items, fn ($item) => !$item->isStatic());

        return array_map(fn ($item) => $item->name, $items);
    }

    /**
     * Get random enum case
     *
     * @return static
     */
    public static function random(): self
    {
        $values = self::cases();

        return $values[rand(0, count($values) - 1)];
    }

    /**
     * Get array of all values with custom method values
     *
     * @return array
     */
    public static function array(): array
    {
        $result = [];
        foreach (self::cases() as $index => $item){
            $result[$index]['value'] = $item->value;

            foreach (self::extraMethods() as $key){
                $result[$index][$key] = $item->$key();
            }
        }

        return $result;
    }

    /**
     * Get json of all values with custom method values
     *
     * @return string
     */
    public static function json(): string
    {
        return json_encode(self::array());
    }
}
